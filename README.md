#Toptal Academy Project How-to

Requires latest version of nodejs and npm.

##1) Install packages:
npm install

##2) Run REST api server:
nodejs server/index.js

##3) Run App
npm start

The following specs were not accomplished:

1) Managers can approve Repairs marked as complete by Users.

2) Users can mark as complete.

3) Repairs cannot overlap. Imagine that the same facility is being used for all repairs.

4) Managers can Create, Read, Edit, and Delete Users and Managers.

5) Users must be able to create an account.



Instead of this I'm providing these test users:

##Test users:
Manager:
user: frivas
password: 1234

User:
user: mlopez
password: 1234