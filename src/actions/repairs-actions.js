// Actions relative to repairs
import axios from 'axios';

export const REPAIRSHOP_REQUEST = 'REPAIRS_REQUEST';
export const REPAIRSHOP_RECEIVE = 'REPAIRS_RECEIVE';
export const REPAIRSHOP_FAIL = 'REPAIRS_FAIL';

const urls = {
  list: 'api/repairshop',
  add: 'api/repairs/add',
  edit: 'api/repairs/edit',
  delete: 'api/repairs/delete',
  complete: 'api/repairs/complete',
  incomplete: 'api/repairs/incomplete',
  addcomment: 'api/comments/add',
};

function request(action, data) {
  return {
    type: REPAIRSHOP_REQUEST,
    action,
    data,
  };
}

function receive(action, data) {
  return {
    type: REPAIRSHOP_RECEIVE,
    action,
    data,
  };
}

function fail(action, data) {
  return {
    type: REPAIRSHOP_FAIL,
    action,
    data,
  };
}

export function fetch(action, data) {
  return (dispatch) => {
    dispatch(request(action, data));
    return axios.post(urls[action], data).then((res) => {
      dispatch(receive(action, res.data));
    }).catch((err) => {
      if (err instanceof Error) {
        // Something happened in setting up the request that triggered an Error
        dispatch(fail(action, err.message));
      } else {
        // The request was made, but the server responded with a status code
        // that falls out of the range of 2xx
        dispatch(fail(action, `Server status${err.status}`));
      }
    });
  };
}

