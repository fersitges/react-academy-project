// Actions relative to repairs
import axios from 'axios';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_RECEIVE = 'LOGIN_RECEIVE';
export const LOGIN_FAIL = 'LOGIN_FAIL';

const urls = {
  loggedin: 'api/loggedin',
  login: 'api/login',
  logout: 'api/logout',
};

function request(action, data) {
  return {
    type: LOGIN_REQUEST,
    action,
    data,
  };
}

function receive(action, data) {
  return {
    type: LOGIN_RECEIVE,
    action,
    data,
  };
}

function fail(action, data) {
  return {
    type: LOGIN_FAIL,
    action,
    data,
  };
}

export function fetch(action, data) {
  return (dispatch) => {
    dispatch(request(action, data));
    return axios.post(urls[action], data).then((res) => {
      dispatch(receive(action, res.data));
    }).catch((err) => {
      if (err instanceof Error) {
        // Something happened in setting up the request that triggered an Error
        dispatch(fail(action, err.message));
      } else {
        // The request was made, but the server responded with a status code
        // that falls out of the range of 2xx
        dispatch(fail(action, `Server status${err.status}`));
      }
    });
  };
}

