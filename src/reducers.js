import { repairshop } from './reducers/repairs-reducer';
import { logincontrol } from './reducers/login-reducers';

export const reducers = {
  logincontrol,
  repairshop,
};

export default reducers;
