import React, { Component } from 'react';
import { Form, Input, Button, Header } from 'semantic-ui-react';

class Login extends Component {
  render() {
    return (
      <Form className="login-form">
        <Header as="h3" textAlign="center">
          Sign in
        </Header>
        <Form.Field id="login" control={Input} label="User" placeholder="User name" />
        <Form.Field id="password" control={Input} label="Password" type="password" />
        <Button type="submit">Login</Button>
        <p>Not registered?</p>
      </Form>
    );
  }
}

export default Login;
