import React, { Component } from 'react';
import update from 'immutability-helper';
import { Modal, Select, Form, Button } from 'semantic-ui-react';
import { mapValues } from 'lodash';

class EditRepair extends Component {
  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
    this.state = {
      open: false,
      users: [],
      repair: {},
      editing: false,
    };
  }

  componentWillReceiveProps(next) {
    let repair = {
      id: false,
      userid: false,
      car: '',
      description: '',
      date: '',
      time: '',
      status: 0,
    };
    if (next.editingRepair === true) {
      repair = next.repair;
    }
    this.setState({
      open: next.open,
      users: next.users,
      repair: repair,
      editing: next.editingRepair,
    });
  }

  close() {
    this.setState({
      open: false,
      editing: false,
      repair: {}
    });
  }

  handleChange = (e, { name, value }) => {
    this.setState({
      repair: update(this.state.repair, {[name]: {$set: value}}),
    });
  };

  addEditRepair() {
    this.setState({ open: false });
    this.props.onAddEditRepair(this.state.repair, this.state.editing);
  }

  render() {
    const { state } = this;
    const options = [];
    mapValues(state.users, (user) => {
      options.push({
        key: user.id,
        text: `${user.name} ${user.lastname}`,
        value: user.id,
      });
    });
    return (
      <Modal
        open={state.open}
        closeOnEscape
        closeOnRootNodeClick
        onClose={this.close}
        closeIcon
      >
        <Modal.Header>
          Add a new repair
        </Modal.Header>
        <Modal.Content image>
          <Modal.Description>
            <Form className="repair-form" onSubmit={this.addEditRepair.bind(this)}>
              <Form.Field
                control={Select}
                label="Select user"
                name="userid"
                options={options}
                placeholder="Select user"
                onChange={this.handleChange}
                value={state.repair.userid}
              />
              <Form.Input name="car" label="Car" placeholder="Car brand and model"
                onChange={this.handleChange}
                value={state.repair.car}
              />
              <Form.Input
                name="description"
                label="Description"
                placeholder="Problem description"
                onChange={this.handleChange}
                value={state.repair.description}
              />
              <Form.Input name="date" label="Date" placeholder="AAAA-mm-dd"
                onChange={this.handleChange}
                value={state.repair.date}
              />
              <Form.Input name="time" label="Time" placeholder="hh:mm"
                onChange={this.handleChange}
                value={state.repair.time}
              />
              <Button type="submit">{state.editing ? 'Update repair' : 'Add repair'}</Button>
            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}

export default EditRepair;
