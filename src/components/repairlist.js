import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, Label, Segment, Message, Dropdown, Menu, Icon, Input, Modal, Button } from 'semantic-ui-react';
import { findIndex } from 'lodash';
import UserComments from './usercomments';
import EditRepair from '../components/edit-repair';

class RepairList extends Component {
  constructor(props) {
    super(props);
    this.handleItemClick = this.handleItemClick.bind(this);
    this.onAddEditRepair = this.onAddEditRepair.bind(this);
    this.editRepair = this.editRepair.bind(this);
    this.clearFilters = this.clearFilters.bind(this);
    this.onAddComment = this.onAddComment.bind(this);
    this.tempcriterium = '';
    this.temptype = '';
    this.state = {
      repairs: [],
      users: [],
      comments: [],
      error: '',
      activeItem: '',
      neworeditrepair: false,
      editingRepair: false,
      repair: {},
      modalOpen: false,
      filter: {
        type: 0,
        criterium: false,
      },
      thisuser: {
        fullname: '',
        role: '',
      },
    };
  }

  componentDidMount() {
    this.props.onInit();
  }

  componentWillReceiveProps(next) {
    this.setState({
      repairs: next.list.repairs,
      users: next.list.users,
      comments: next.list.comments,
      error: next.error,
      thisuser: next.list.user,
    });
  }

  onAddEditRepair(repair, editing) {
    this.setState({ neworeditrepair: false });
    this.props.onAddEditRepair(repair, editing);
  }

  setFilter(type, criterium) {
    if (type <= 3) {
      this.temptype = type;
      this.setState({
        modalOpen: true,
      });
    } else {
      this.setState({
        filter: {
          type,
          criterium,
        },
      });
      this.props.onSetFilter(type, criterium);
    }
  }

  handleItemClick(e, { name }) {
    const prevstate = { activeItem: name };
    if (name === 'Add repair') {
      prevstate.neworeditrepair = true;
      prevstate.editingRepair = false;
    }
    this.setState(prevstate);
  }

  deleteRepair(repairid) {
    this.props.onDeleteRepair(repairid);
  }

  markComplete(repairid) {
    this.props.onMarkComplete(repairid);
  }

  markIncomplete(repairid) {
    this.props.onMarkIncomplete(repairid);
  }

  editRepair(repair) {
    this.setState({
      neworeditrepair: true,
      editingRepair: true,
      repair,
    });
  }

  clearFilters() {
    this.setState({
      modalOpen: false,
      filter: {
        type: 0,
        criterium: '',
      },
    });
    this.props.onSetFilter(0, '');
  }

  handleClose = () => this.setState({ modalOpen: false });

  onSetModalFilter = () => {
    this.setState({
      modalOpen: false,
      filter: {
        type: this.temptype,
        criterium: this.tempcriterium,
      }
    });
    this.props.onSetFilter(this.temptype, this.tempcriterium);
  };

  handleFilterChange = (event) => {
    this.tempcriterium = event.target.value;
  };

  onAddComment(comment, repairid) {
    this.props.onAddComment(comment, repairid);
  }

  render() {
    const { state } = this;
    const { activeItem } = this.state;
    return (
      <Segment>
        <Menu>
          <Menu.Item header>List of Repairs</Menu.Item>
          {
            state.thisuser.role === 'manager' &&
            <Menu.Item
              name="Add repair"
              active={activeItem === 'Add repair'}
              onClick={this.handleItemClick}
            >
            <Icon name="plus" /> Add repair
          </Menu.Item>
          }
          <Menu.Menu position="left">
            <Dropdown item text="Filter repairs" onOpen={this.focus}>
              <Dropdown.Menu>
                <Dropdown.Item onClick={() => this.setFilter(1)}>
                  {state.filter.type === 1 ? <Icon name="check" /> : false }
                  Date
                </Dropdown.Item>
                <Dropdown.Item onClick={() => this.setFilter(2)}>
                  {state.filter.type === 2 ? <Icon name="check" /> : false }
                  Time
                </Dropdown.Item>
                <Dropdown.Item onClick={() => this.setFilter(3)}>
                  {state.filter.type === 3 ? <Icon name="check" /> : false }
                  User
                </Dropdown.Item>
                <Dropdown.Item onClick={() => this.setFilter(4)}>
                  {state.filter.type === 4 ? <Icon name="check" /> : false }
                  Complete
                </Dropdown.Item>
                <Dropdown.Item onClick={() => this.setFilter(5)}>
                  {state.filter.type === 5 ? <Icon name="check" /> : false }
                  Incomplete
                </Dropdown.Item>
                <Dropdown.Item onClick={this.clearFilters}>
                  Clear filters
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
          {
            state.thisuser.role === 'manager' &&
            <Menu.Item name="Manage users" />
          }
        </Menu>
        <List divided relaxed>
          {
            state.repairs.map((singlerepair) => {
              const userindex = findIndex(state.users, { id: singlerepair.userid });
              const username = `${state.users[userindex].name} ${state.users[userindex].lastname}`;
              return (
                <List.Item key={singlerepair.id}>
                  <List.Icon name="configure" size="large" verticalAlign="top" />
                  <List.Content>
                    <List.Header as="h3">{username}</List.Header>
                    <List.Description>
                      <Label color="teal">{ singlerepair.car }</Label>
                      <Label>{ singlerepair.date }</Label>
                      <Label>{ singlerepair.time }</Label>
                      <Label color={singlerepair.status === 0 ? 'red' : 'green'}>{
                        ['Incomplete', 'Pending approval', 'Complete'][singlerepair.status]
                      }</Label>
                      <Label color="orange" basic>
                        <Dropdown floating text="Actions">
                          <Dropdown.Menu>
                            {
                              state.thisuser.role === 'manager' &&
                              <Dropdown.Item
                                text="Edit"
                                onClick={() => this.editRepair(singlerepair)}
                              />
                            }
                            {
                              state.thisuser.role === 'manager' &&
                              <Dropdown.Item
                                text="Delete"
                                onClick={() => this.deleteRepair(singlerepair.id)}
                              />
                            }
                            <Dropdown.Item
                              text={state.thisuser.role === 'manager' ? 'Complete' : 'Mark as complete'}
                              onClick={() => this.markComplete(singlerepair.id)}
                            />
                            {
                              state.thisuser.role === 'manager' &&
                              <Dropdown.Item
                                text="Incomplete"
                                onClick={() => this.markIncomplete(singlerepair.id)}
                              />
                            }
                          </Dropdown.Menu>
                        </Dropdown>
                      </Label>
                    </List.Description>
                  </List.Content>
                  <List.Content className="problem">
                    <Message>
                      <Message.Header>
                        Problem description
                      </Message.Header>
                      <p>{singlerepair.description}</p>
                    </Message>
                    <UserComments
                      comments={state.comments}
                      repairid={singlerepair.id}
                      users={state.users}
                      onAddComment={this.onAddComment}
                    />
                  </List.Content>
                </List.Item>
              );
            })
          }
        </List>
        <EditRepair
          open={state.neworeditrepair}
          users={state.users}
          onAddEditRepair={this.onAddEditRepair}
          editingRepair={state.editingRepair}
          repair={state.repair}
        />
        <Modal
          closeOnEscape
          closeOnRootNodeClick
          closeIcon
          onClose={this.handleClose}
          open={this.state.modalOpen}
          size='tiny'>
          <Modal.Content>
            <h3>Enter filter</h3>
            <Input
              icon="filter"
              iconPosition="left"
              name="filter"
              placeholder={[
                'Enter a date',
                'Enter a time',
                'Enter full or partial user name'
              ][this.temptype - 1]}
              type={['date', 'time', 'text'][this.temptype - 1]}
              focus
              onChange={this.handleFilterChange}
            />
          </Modal.Content>
          <Modal.Actions>
            <Button
              primary
              onClick={() => this.onSetModalFilter()}>
                Apply filter
            </Button>
          </Modal.Actions>
        </Modal>
      </Segment>
    );
  }
}

RepairList.propTypes = {
  onInit: PropTypes.func.isRequired,
  onAddEditRepair: PropTypes.func.isRequired,
  onDeleteRepair: PropTypes.func.isRequired,
  onSetFilter: PropTypes.func.isRequired,
  onMarkComplete: PropTypes.func.isRequired,
  onMarkIncomplete: PropTypes.func.isRequired,
  onAddComment: PropTypes.func.isRequired,
  list: PropTypes.object.isRequired,
//  error: PropTypes.string.isRequired,
};

export default RepairList;
