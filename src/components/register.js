import React, { Component } from 'react';
import { Form, Button, Header } from 'semantic-ui-react';

class Register extends Component {
  render() {
    return (
      <Form className="login-form">
        <Header as="h3" textAlign="center">
          Register
        </Header>
        <Form.Group widths="equal">
          <Form.Input id="first-name" label="First name" placeholder="First name" />
          <Form.Input id="last-name" label="Last name" placeholder="Last name" />
        </Form.Group>
        <Form.Input id="login" label="User name" placeholder="User you will use to login" />
        <Form.Input id="password" label="Password" placeholder="Choose a password" />
        <Form.Input id="password-confirm" label="Confirm your password" />
        <Button type="submit">Register</Button>
      </Form>
    );
  }
}

export default Register;
