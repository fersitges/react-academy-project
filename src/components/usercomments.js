import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findIndex } from 'lodash';
import { Comment, Header, Button, Segment, Modal, TextArea } from 'semantic-ui-react';

class UserComments extends Component {
  constructor(props) {
    super(props);
    this.tempcomment = '';
    this.onAddComment = this.onAddComment.bind(this);
    this.state = {
      comments: props.comments,
      repairid: props.repairid,
      users: props.users,
      modalOpen: false,
    };
  }

  componentWillReceiveProps(next) {
    this.setState({
      comments: next.comments,
      repairid: next.repairid,
      users: next.users,
    })
  }

  openCommentsModal = () => {
    this.setState({
      modalOpen: true,
    });
  };

  handleClose = () => this.setState({ modalOpen: false });

  handleCommentChange = (event) => {
    this.tempcomment = event.target.value;
  };

  onAddComment() {
    this.setState({ modalOpen: false });
    this.props.onAddComment(this.tempcomment, this.state.repairid);
  }

  render() {
    const { state } = this;
    const commentsbyid = this.state.comments.filter(
      comment => comment.repairid === this.state.repairid,
    );
    return (
      <Segment color="blue">
        <Comment.Group>
          <Header as="h3" dividing>Comments</Header>
          {
            commentsbyid.map((singlecomment) => {
              const userindex = findIndex(state.users, { id: singlecomment.userid });
              const username = `${state.users[userindex].name} ${state.users[userindex].lastname}`;
              return (
                <Comment key={singlecomment.id}>
                  <Comment.Content>
                    <Comment.Author>{username}</Comment.Author>
                    <Comment.Metadata>
                      <div>{singlecomment.datetime}</div>
                    </Comment.Metadata>
                    <Comment.Text>
                      {singlecomment.comment}
                    </Comment.Text>
                  </Comment.Content>
                </Comment>
              );
            })
          }
          <Button
            content="Add comment"
            icon="plus square"
            labelPosition="right"
            onClick={this.openCommentsModal}
          />
        </Comment.Group>
        <Modal
          closeOnEscape
          closeOnRootNodeClick
          closeIcon
          onClose={this.handleClose}
          open={this.state.modalOpen}
          size='tiny'>
          <Modal.Content>
            <h3>Add a comment</h3>
            <TextArea
              name="comment"
              placeholder="Write a comment about this repair"
              onChange={this.handleCommentChange}
            />
          </Modal.Content>
          <Modal.Actions>
            <Button
              primary
              onClick={this.onAddComment}>
              Add
            </Button>
          </Modal.Actions>
        </Modal>
      </Segment>
    );
  }
}

UserComments.propTypes = {
  comments: PropTypes.array.isRequired,
  repairid: PropTypes.number.isRequired,
  users: PropTypes.array.isRequired,
};

export default UserComments;
