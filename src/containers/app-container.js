import { connect } from 'react-redux';
import App from '../App';
import { fetch } from '../actions/login-actions';

const mapStateToProps = state => ({
  user: state.logincontrol.user,
  error: state.logincontrol.error,
});

const mapDispatchToProps = dispatch => ({
  onInit: () => {
    dispatch(fetch('loggedin'));
  },
  onLoginSubmit: (logindata) => {
    dispatch(fetch('login', logindata));
  },
  onLogout: () => {
    dispatch(fetch('logout'));
  },
});

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

export default AppContainer;
