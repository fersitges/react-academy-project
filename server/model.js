// Models
// Users
exports.users = [
  {
    id: 0,
    name: 'Fernando',
    lastname: 'Rivas',
    role: 'manager',
    password: '1234',
    username: 'frivas',
  },
  {
    id: 1,
    name: 'Martha',
    lastname: 'Lopez',
    role: 'user',
    password: '1234',
    username: 'mlopez',
  },
  {
    id: 2,
    name: 'Brad',
    lastname: 'Pitt',
    role: 'user',
    password: '1234',
    username: 'bpitt',
  },
];

// Repairs
exports.repairs = [
  {
    id: 0,
    userid: 1,
    car: 'Ford Fiesta',
    description: 'Air condition not working',
    status: 0,
    date: '2017-08-06',
    time: '14:00',
  },
  {
    id: 1,
    userid: 1,
    car: 'Volkswagen Beetle',
    description: 'Makes weird buzz beetle-like sound',
    status: 0,
    date: '2017-08-06',
    time: '14:00',
  },
  {
    id: 2,
    userid: 2,
    car: 'Juke Cristina',
    description: 'Its quite red',
    status: 2,
    date: '2017-09-23',
    time: '15:00',
  },
];

// Comments
exports.comments = [
  {
    id: 0,
    userid: 1,
    repairid: 0,
    datetime: '2017-10-10 9:30:00',
    comment: '0 Cum ionicis tormento peregrinatione, omnes devirginatoes visum audax, peritus lunaes?',
  },
  {
    id: 1,
    userid: 1,
    repairid: 0,
    datetime: '2017-10-11 11:30:00',
    comment: '0 Cum lixa peregrinatione, omnes accentores imitari regius, bi-color fermiumes?',
  },
  {
    id: 2,
    userid: 0,
    repairid: 1,
    datetime: '2017-10-12 12:30:00',
    comment: '1 Mirabilis, raptus acipensers nunquam transferre de camerarius, bassus fraticinida?',
  },
  {
    id: 3,
    userid: 1,
    repairid: 1,
    datetime: '2017-10-13 13:30:00',
    comment: '1 Lotus, domesticus caniss una anhelare de flavum, fatalis uria?',
  },
];
