const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api');

const port = 3001;
const app = express();

app.use(bodyParser.json());
app.use('/api', api);
app.use(express.static(path.join(__dirname, '../public')));
app.listen(port);
console.log(`react test server listening on ${port}`);
